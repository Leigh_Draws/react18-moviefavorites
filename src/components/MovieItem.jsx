import PropTypes from "prop-types";
import { useState } from "react";
import "./MovieItem.css";

function MovieItem(props) {
  const { title, released, director, poster } = props;

  const currentFavoriteValue = localStorage.getItem("NewStateFavorite") === "true" ? true : false;
  // Créer un état favoris et une fonction "setFavorite", état initial à "false"
  const [favorite, setFavorite] = useState(currentFavoriteValue);

 function toggleFavorite() {
  const newFavorite = !favorite;
  setFavorite(newFavorite);

  localStorage.setItem("NewStateFavorite", newFavorite)
 }

  return (
    <div className="MovieItem">
      <h2>{title}</h2>
      <img src={poster} alt={title} />
      <h4>Director: {director}</h4>
      <h5>Released: {released}</h5>
    {/* Action au clic, fonction fléchée, change l'état du favoris à l'inverse de ce qu'il était avec !Favorite */}
      <button type="button" onClick={toggleFavorite}>{favorite ? "Retirer des favoris" : "Ajouter aux favoris"}</button>
    {/* A l'interieur du bouton, si l'état de favoris est vrai on écrit "retirer du favoris", s'il est faux "ajouter aux favoris" */}
    </div>
  );
}

MovieItem.propTypes = {
  title: PropTypes.string.isRequired,
  released: PropTypes.string.isRequired,
  director: PropTypes.string.isRequired,
  poster: PropTypes.string.isRequired,
};

export default MovieItem;
